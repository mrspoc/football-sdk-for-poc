import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import {Team} from "./constants/Types";
import {FOOTBALL_API_KEY, REQUEST_TIMEOUT} from "./constants/Constants";

export const TextFunction = (text: string) => `Hello ${text}`;

const DEFAULT_REQUEST_CONFIG: AxiosRequestConfig = {
    timeout: REQUEST_TIMEOUT,
    headers: {
        'X-Auth-Token': FOOTBALL_API_KEY
    }
}

export const getAllTeams = (): Promise<AxiosResponse<Team[]>> =>
    new Promise(async (resolve, reject) => {
        try {
            const url = 'http://api.football-data.org/v2/competitions/2000/teams';
            const res = await axios.get(url, DEFAULT_REQUEST_CONFIG);
            if (res?.status === 200) {
                resolve(res?.data);
            } else {
                reject(`rejected teams request manually coz of status: ${res?.status}`);
            }
        } catch (error) {
            reject(error);
        }
    });

